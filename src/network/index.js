import axiosLib from 'axios';
import BASE_URL from '@/constants/';
import AxiosConfigurator from './AxiosConfigurator';

import * as mocks from './mocks';

const axiosConfigurator = new AxiosConfigurator(axiosLib);

const isDevelopment = true;
if (isDevelopment) {
  const mocksList = Object.values(mocks);
  axiosConfigurator.mock(mocksList);
}

const apiConfigurator = axiosConfigurator.create({
  baseURL: BASE_URL,
});

export default apiConfigurator.eject();
