import BASE_URL from '@/constants';

export const getFlyers = {
  url: `${BASE_URL}/v1/products/flyers`,
  method: 'GET',
  responseFn: async () => [200, await import('./flyers.json')],
};

export const getBusinessCards = {
  url: `${BASE_URL}/v1/products/business-cards`,
  method: 'GET',
  responseFn: async () => [200, await import('./businesscards.json')],
};

export const getPosters = {
  url: `${BASE_URL}/v1/products/posters`,
  method: 'GET',
  responseFn: async () => [200, await import('./posters.json')],
};
