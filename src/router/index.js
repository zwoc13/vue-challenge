import Vue from 'vue';
import VueRouter from 'vue-router';

import HomeView from '@/views/HomeView';
import ProductView from '@/views/ProductView';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'HomeView',
    component: HomeView,
  },
  {
    path: '/product/:category',
    name: 'ProductView',
    component: ProductView,
  },
];

const router = new VueRouter({
  routes,
  scrollBehavior() {
    return {
      top: 0,
    };
  },
});

export default router;
