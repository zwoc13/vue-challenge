export default [{
  id: 1,
  name: 'Flyers',
  url: '/product/flyers',
  image: 'https://www.print.com/en/wp-content/uploads/2021/03/image-2.jpg',
}, {
  id: 2,
  name: 'Business Cards',
  url: '/product/business-cards',
  image: 'https://www.print.com/en/wp-content/uploads/2021/03/image-3-1280x939.jpg',
}, {
  id: 3,
  name: 'Posters',
  url: '/product/posters',
  image: 'https://www.print.com/en/wp-content/uploads/2021/03/MGL3740-1280x853.jpg',
}];
