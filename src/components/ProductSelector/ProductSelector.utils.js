export default ({ optionSlug, propertySlug }, exclusionRules) => {
  const filteredRules = exclusionRules.filter((rulesSet) => rulesSet.some((rule) => {
    const { options, property } = rule;
    const isSelectedOptionInside = options.includes(optionSlug);
    return property === propertySlug && isSelectedOptionInside;
  }));

  if (!filteredRules.length) return [];

  const flattenedRules = filteredRules.flat();

  const nonPropertyRules = flattenedRules.filter((rule) => {
    const { property } = rule;
    return property !== propertySlug;
  });

  // what? If we apply offset rule then there are many arrays
  // with materials that cannot be applied?
  const uniqueRulesList = [];
  // for-let is faster then .forEach
  for (let i = 1; i < nonPropertyRules.length; i += 1) {
    const rule = nonPropertyRules[i];
    const { options, property } = rule;
    const isRuleInUniqueList = uniqueRulesList.some((ruleItem) => {
      const { property: propertyToCheck } = ruleItem;
      return propertyToCheck === property;
    });

    if (isRuleInUniqueList) {
      const ruleInArray = uniqueRulesList.find((ruleItem) => {
        const { property: propertyToCheck } = ruleItem;
        return propertyToCheck === property;
      });

      const mergedOptions = [...new Set([...options, ...ruleInArray.options])];
      ruleInArray.options = mergedOptions;
    } else {
      uniqueRulesList.push(rule);
    }
  }

  return uniqueRulesList;
};
