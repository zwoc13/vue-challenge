import Vue from 'vue';
import Vuex from 'vuex';

import productStore from '@/store/productStore';
import cartStore from '@/store/cartStore';
import selectorStore from '@/store/selectorStore';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
    selectorStore,
    productStore,
    cartStore,
  },
});
