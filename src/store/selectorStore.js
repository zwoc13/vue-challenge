export default {
  namespaced: true,

  state: () => ({
    sku: null,
    options: [],
    excludedOptions: [],
    amount: 0,
  }),

  getters: {
    sku: (state) => state.sku,
    selectedOptions: (state) => state.options,
    excludedOptions: (state) => state.excludedOptions,
    amount: (state) => state.amount,
  },

  mutations: {
    setSKU(state, payload) {
      state.sku = payload;
    },

    addExcludedOptions(state, payload) {
      const exclusionsList = state.excludedOptions.concat(payload);
      state.excludedOptions = exclusionsList;
    },

    addOption(state, payload) {
      const isRecordAlreadyInState = state.options.some((option) => {
        const { propertySlug } = option;
        return propertySlug === payload.propertySlug;
      });

      if (isRecordAlreadyInState) {
        const optionsCopy = [...state.options];
        const objectIndex = optionsCopy.findIndex((option) => {
          const { propertySlug } = option;
          return propertySlug === payload.propertySlug;
        });

        optionsCopy[objectIndex] = payload;

        state.options = optionsCopy;
      } else {
        const updatedOptionsList = state.options.concat([payload]);
        state.options = updatedOptionsList;
      }
    },

    setAmount(state, payload) {
      state.amount = payload;
    },
  },

  actions: {
    setSKU({ commit }, sku) {
      commit('setSKU', sku);
    },

    addExcludedOptions({ commit }, excludedOptions) {
      commit('addExcludedOptions', excludedOptions);
    },

    selectOption({ commit }, optionToAdd) {
      commit('addOption', optionToAdd);
    },

    setAmount({ commit }, amount) {
      commit('setAmount', amount);
    },
  },
};
