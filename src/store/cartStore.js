export default {
  namespaced: true,

  state: () => ({
    products: [],
  }),

  getters: {
    productsInCart: (state) => state.products,
    totalAmount: (state) => {
      const { products } = state;
      const total = products.reduce(
        (previousValue, currentItem) => previousValue + currentItem.price,
        0,
      );

      return total;
    },
  },

  mutations: {
    addProductToCart(state, payload) {
      const modifiedCart = state.products.concat(payload);
      state.products = modifiedCart;
    },

    removeProductFromCart(state, payload) {
      const filteredCart = state.products.filter((product) => product.id !== payload);
      state.products = filteredCart;
    },

    clearCart(state) {
      state.products = [];
    },
  },

  actions: {
    addProductToCart({ commit }, product) {
      commit('addProductToCart', product);
    },

    removeProductFromCart({ commit }, productId) {
      commit('removeProductFromCart', productId);
    },

    clearCart({ commit }) {
      commit('clearCart');
    },
  },
};
