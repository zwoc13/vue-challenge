import apiClient from '@/network';

export default {
  namespaced: true,

  state: () => ({
    product: {},

    isProductLoading: false,
  }),

  getters: {
    titles: (state) => ({
      single: state.product.titleSingle,
      plural: state.product.titlePlural,
    }),
    isProductActive: (state) => state.product.active,

    product: (state) => state.product,
    exclusions: (state) => state.product.excludes,
    maxDesigns: (state) => state.product.maxDesigns,

    isProductLoading: (state) => state.isProductLoading,
  },

  mutations: {
    setProduct(state, payload) {
      state.product = payload;
    },
  },

  actions: {
    async getProductInformation({ commit }, category) {
      try {
        const { data: { default: product } } = await apiClient.get(`/v1/products/${category}`);
        commit('setProduct', product);
      } catch (error) {
        // TODO: Should we apply Sentry in this case?
        console.error(error);
      }
    },
  },
};
