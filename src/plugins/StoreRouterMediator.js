const syncCartWithLocalStorage = (updatedCart) => {
  localStorage.setItem('printCom/cart', updatedCart);
};

export default {
  install(Vue, { store }) {
    // Basically that's the mediator when we need to combine both
    store.watch(
      (state, getters) => {
        const productsInCart = getters['cartStore/cart'];
        return productsInCart;
      },
      (updatedCart) => syncCartWithLocalStorage(updatedCart),
    );
  },
};
