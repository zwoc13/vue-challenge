import Vue from 'vue';
import StoreRouterMediator from '@/plugins/StoreRouterMediator';
import App from './App.vue';
import router from './router';
import store from './store';

import './directives/clickOutside';
import './assets/scss/main.scss';

Vue.config.productionTip = false;

Vue.use(StoreRouterMediator, {
  store, router,
});

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
